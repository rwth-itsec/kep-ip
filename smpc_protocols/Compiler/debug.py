from Compiler.library import print_ln, print_str, for_range


def print_matrix(matrix, rows, cols):
    @for_range(rows)
    def _(i):
        @for_range(cols)
        def _(j):
            print_str("%s ", matrix[i][j].reveal())

        print_ln(" ")
