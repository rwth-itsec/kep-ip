# coding: latin-1
"""

"""

from Compiler.types import sint, regint, Array, MemValue, Matrix, MultiArray
from Compiler.library import print_ln, do_while, for_range, print_str, if_, for_range_parallel
from Compiler.networking import write_output_to_clients, client_input, accept_client

BLOOD_TYPES = 4
ANTIGEN_TYPES = 59 + 132 + 48 + 61 + 26 + 22
ANTIGEN_TYPES_A = 59
ANTIGEN_TYPES_B = 132
ANTIGEN_TYPES_C = 48
ANTIGEN_TYPES_DR = 61
ANTIGEN_TYPES_DQ = 26
ANTIGEN_TYPES_DP = 22

SIZE_COMP_INPUT = 2 * BLOOD_TYPES + 2 * ANTIGEN_TYPES


def compute_compatibility(blood_donor, antigen_donor, blood_patient, antigen_patient):
    sumb = Array(1, sint)
    sumb[0] = sint(0)

    suma = Array(1, sint)
    suma[0] = sint(0)

    sumb[0] = sint.dot_product(blood_patient, blood_donor)

    suma[0] = sint.dot_product(antigen_patient, antigen_donor)

    ohb = sint(0) < sumb[0]
    oha = suma[0] < sint(1)
    return ohb * oha


def compute_comp_matrix(blood_donor, blood_patient, antigen_donor, antigen_patient, num_clients):
    adjacency_matrix = sint.Matrix(num_clients, num_clients)
    adjacency_matrix.assign_all(sint(0))

    @for_range_parallel(num_clients, num_clients)
    def _(client_i):
        @for_range_parallel(num_clients, num_clients)
        def _(client_j):
            adjacency_matrix[client_i][client_j] = compute_compatibility(blood_donor[client_i],
                                                                         antigen_donor[client_i],
                                                                         blood_patient[client_j],
                                                                         antigen_patient[client_j])

    return adjacency_matrix


def read_input(num_clients):
    blood_donor = sint.Matrix(num_clients, BLOOD_TYPES)
    blood_patient = sint.Matrix(num_clients, BLOOD_TYPES)
    antigen_donor = sint.Matrix(num_clients, ANTIGEN_TYPES)
    antibodies_patient = sint.Matrix(num_clients, ANTIGEN_TYPES)

    @for_range(num_clients)
    def _(client_id):
        blood_donor[client_id] = client_input(client_id, BLOOD_TYPES)
        blood_patient[client_id] = client_input(client_id, BLOOD_TYPES)

    @for_range(num_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_A)
        for i in range(ANTIGEN_TYPES_A):
            antigen_donor[client_id][i] = tmp[i]

    @for_range(num_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_A)

        for i in range(ANTIGEN_TYPES_A):
            antibodies_patient[client_id][i] = tmp[i]

    @for_range(num_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_B)

        for i in range(ANTIGEN_TYPES_B):
            antigen_donor[client_id][ANTIGEN_TYPES_A + i] = tmp[i]

    @for_range(num_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_B)

        for i in range(ANTIGEN_TYPES_B):
            antibodies_patient[client_id][ANTIGEN_TYPES_A + i] = tmp[i]

    @for_range(num_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_C)

        for i in range(ANTIGEN_TYPES_C):
            antigen_donor[client_id][ANTIGEN_TYPES_A + ANTIGEN_TYPES_B + i] = tmp[i]

    @for_range(num_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_C)

        for i in range(ANTIGEN_TYPES_C):
            antibodies_patient[client_id][ANTIGEN_TYPES_A + ANTIGEN_TYPES_B + i] = tmp[i]

    @for_range(num_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_DR)

        for i in range(ANTIGEN_TYPES_DR):
            antigen_donor[client_id][ANTIGEN_TYPES_A + ANTIGEN_TYPES_B + ANTIGEN_TYPES_C + i] = \
                tmp[
                    i]

    @for_range(num_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_DR)

        for i in range(ANTIGEN_TYPES_DR):
            antibodies_patient[client_id][ANTIGEN_TYPES_A + ANTIGEN_TYPES_B + ANTIGEN_TYPES_C + i] = \
                tmp[i]

    @for_range(num_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_DQ)

        for i in range(ANTIGEN_TYPES_DQ):
            antigen_donor[client_id][
                ANTIGEN_TYPES_A + ANTIGEN_TYPES_B + ANTIGEN_TYPES_C + ANTIGEN_TYPES_DR + i] = \
                tmp[
                    i]

    @for_range(num_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_DQ)

        for i in range(ANTIGEN_TYPES_DQ):
            antibodies_patient[client_id][
                ANTIGEN_TYPES_A + ANTIGEN_TYPES_B + ANTIGEN_TYPES_C + ANTIGEN_TYPES_DR + i] = \
                tmp[i]

    @for_range(num_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_DP)

        for i in range(ANTIGEN_TYPES_DP):
            antigen_donor[client_id][
                ANTIGEN_TYPES_A + ANTIGEN_TYPES_B + ANTIGEN_TYPES_C + ANTIGEN_TYPES_DR + ANTIGEN_TYPES_DQ + i] = \
                tmp[i]

    @for_range(num_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_DP)

        for i in range(ANTIGEN_TYPES_DP):
            antibodies_patient[client_id][
                ANTIGEN_TYPES_A + ANTIGEN_TYPES_B + ANTIGEN_TYPES_C + ANTIGEN_TYPES_DR + ANTIGEN_TYPES_DQ + i] = \
                tmp[i]

    return blood_donor, blood_patient, antigen_donor, antibodies_patient
